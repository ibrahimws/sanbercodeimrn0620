//NO. 1 Array to Object
console.log("NO. 1 Array to Object");

function arrayToObject(arr) {
    var org = {}
    var now = new Date()
    var j;
    var thisYear = now.getFullYear() // 2020 (tahun sekarang
    for (var i = 0; i < arr.length; i++) {
        org.firstName   = arr[i][0]
        org.lastName    = arr[i][1]
        org.gender      = arr[i][2]
        if ((arr[i][3]==null) || (arr[i][3] > thisYear)) {
            org.age = "Invalid birth year"
        }
        else {

        org.age         = thisYear - arr[i][3]
        }
        j=i+1;
        console.log(j+". " + org.firstName + " " + org.lastName + ": ")
        console.log(org)
    }
    
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//NO. 2 Array to Object
console.log("NO. 2 Shopping Time");

function shoppingTime(memberId, money) {
    var trx = {}
    var list = []
    var uangSaatIni = money
    if ((memberId == null) || (memberId == "")) {
        return "Mohon maat, toko X hanya berlaku untuk member saja"
    }
    else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        trx.memberId = memberId
        trx.money = money

        if (uangSaatIni>=1500000)
        {
            list.push("Sepatu Stacattu")
            uangSaatIni = uangSaatIni-1500000
        }
        if (uangSaatIni>=500000)
        {
            list.push("Baju Zoro")
            uangSaatIni = uangSaatIni-500000
        }
        if (uangSaatIni>=250000)
        {
            list.push("Baju H&N")
            uangSaatIni = uangSaatIni-250000
        }
        if (uangSaatIni>=175000)
        {
            list.push("Sweater Uniklook")
            uangSaatIni = uangSaatIni-175000
        }
        if (uangSaatIni>=50000)
        {
            list.push("Casing Handphone")
            uangSaatIni = uangSaatIni-50000
        }
        trx.listPurchased = list
        trx.changeMoney = uangSaatIni
        return trx
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//NO. 3 Naik Angkot
console.log("NO. 3 Naik Angkot");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var penumpang = [];
  if (arrPenumpang != null) {
    for (var i=0; i<arrPenumpang.length; i++)
    {
        var ruteUser = rute.slice(
            rute.indexOf(arrPenumpang[i][1]),
            rute.indexOf(arrPenumpang[i][2])
          );
        var bayar = 2000 * ruteUser.length;
        var data = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
        bayar: bayar,
        };
        
        penumpang.push(data);
        data = {};
    }
    return penumpang
    }
    
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]