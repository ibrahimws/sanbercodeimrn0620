//SOAL NO 1
console.log('LOOPING PERTAMA');
var deret = 2;
while(deret <= 20) 
{
    console.log(deret + ' - I love coding');
    deret += 2; 
}
console.log('LOOPING KEDUA');
var deret = 20;
while(deret >= 2) 
{
    console.log(deret + ' - I will become a mobile developer');
    deret -= 2; 
}

//SOAL NO 2
console.log('SOAL NO 2');
var cekangka;
for(var angka = 1; angka <= 20; angka++) 
{
    if ( (angka%2 == 1) && (angka%3 == 0)) 
    {
        console.log(angka + ' - I Love Coding');
    }
    else if ( angka%2 == 1) 
    {
        console.log(angka + ' - Santai');    
    }
    else if ( angka%2 == 0) 
    {
        console.log(angka + ' - Berkualitas');
    }
}
//SOAL NO 3
console.log('SOAL NO 3');
var tulis = '';
for(var deret = 1; deret <= 4; deret++) 
{
    for(var angka = 1; angka <= 8; angka++) 
    {
      tulis += '#';
    }
    console.log(tulis);
    tulis = '';
}

//SOAL NO 4
console.log('SOAL NO 4');
var tulis = '';
for(var deret = 1; deret <= 7; deret++) 
{
    for(var angka = 1; angka <= 1; angka++) 
    {
      tulis += '#';
    }
    console.log(tulis);
}

//SOAL NO 5
console.log('SOAL NO 5');
var tulis = '';
for(var deret = 1; deret <= 8; deret++) 
{
    cekbilangan = deret%2;
    if (cekbilangan == 1)
    {
        tulis += ' ';
    }
    for(var angka = 1; angka <= 4; angka++) 
    {
      tulis += '# ';
    }
    console.log(tulis);
    tulis = '';
}