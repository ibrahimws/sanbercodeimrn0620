import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  Image,
  Button,
  ActivityIndicator,
  TouchableHighlightBase,

} from 'react-native';
import {connect} from "react-redux";

import {logoutUser} from "../actions/auth.actions";
import {Actions} from 'react-native-router-flux';
import Axios from 'axios';


const DEVICE = Dimensions.get('window')

const styles = StyleSheet.create({
  header:{
    width:360,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'azure',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  itemContainer: {
    width: DEVICE.width * 0.44,
    margin: 8,
    padding: 6,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 16,
  },
  itemImage: {
    marginTop: 5,
    width: 80,
    height: 64,
  },
  itemName: {
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
    fontSize: 12,
  },
  itemPrice: {
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 12,
  },
  itemStock: {
    fontSize: 8,
    marginBottom: 5,
  },
  button: {
    width:120,
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 5,
    paddingVertical: 5
  },
  buttonText: {
    fontSize:12,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
});



class Home extends Component<{}> {

  constructor(props) {
    super(props)
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
      searchText: '',
      totalPrice: 0,
    }
  }

  //mount data
  componentDidMount() {
    this.getProduk()
  }

  // Get api
  getProduk = async () => {
    try {
      const response = await Axios.get(`http://hiimtutor.com/api`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    }
    catch (error)
    {
      this.setState({ isLoading: false, isError: true })
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {

    let totalPrice = this.state.totalPrice;
    this.setState({
      totalPrice: totalPrice + price,
    })
  }

  logoutUser = () => {
      this.props.dispatch(logoutUser());
  }

  profile() {
    Actions.profile()
}

  detail() {
      Actions.detail()
  }

 

	render() {
    const {getUser: {userDetails}} = this.props;
    if (this.state.isLoading) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    else if (this.state.isError) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }

		return(
			<View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity style={styles.button} onPress={this.logoutUser}>
             <Text style={styles.buttonText}>Logout</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.button} onPress={this.profile}>
             <Text style={styles.buttonText}>Profile</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.button} onPress={this.detail}>
             <Text style={styles.buttonText}>Detail</Text>
           </TouchableOpacity>
          </View>
          <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
             <Text style={styles.headerText}></Text>
            </Text>

           <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
            <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', padding:5,marginTop:5, }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        <FlatList 
          data={this.state.data}
          numColumns={2}
          renderItem={ ({item}) => <ListItem data={item} detailProduk={this.detail} onBuy={this.updatePrice.bind(this) } /> }
        />
      
			</View>
      
			)
	}
}

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <TouchableOpacity onPress={() => this.props.detailProduk()}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain'  />
        </TouchableOpacity>
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button style={styles.itemButton} title='BELI' color='#6495ED' onPress={() => this.props.onBuy(Number(data.harga))} />
      </View>
    )
  }
};

mapStateToProps = (state) => ({
    getUser: state.userReducer.getUser
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
