import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {connect} from "react-redux";
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fa from 'react-native-vector-icons/AntDesign';

import {logoutUser} from "../actions/auth.actions";
import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  header:{
    width:360,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cheader:{
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:360,
  },
  content:{
  	height:600,
  	padding:20
  },
  box1:{
    marginTop:10,
    backgroundColor:'#EFEFEF',
    height:150,
    width:320,
    borderRadius:10,

  },
  box2:{
    marginTop:10,
    backgroundColor:'#EFEFEF',
    height:200,
    width:320,
    borderRadius:10,

  },
  people:{
    textAlign:'center',
    width:320,
    top:-20,
    margin: 0,
    padding : 0,
    color : '#EFEFEF'
  },
  identity:{
    textAlign:'center',
    color:'#003366',
    top : -10,
    fontSize : 24,
    fontWeight: 'bold',
  },
  identity2:{
    textAlign:'left',
    marginLeft:10,
    color:'#003366'
  },
  identity3:{
    textAlign:'center',
    color:'#3EC6FF',
    fontSize : 16,
    top : -10
  },
  page1:{
    width:360,
    height:150,
    padding:10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  page2:{
    width:360,
    height:150,
    left : 90,
    top : 10,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  satu:{
    alignItems: 'center',
    width:170,
    height:150,
    padding:10,
    left : -30,
    textAlign:'center'
  },
  mendatar:{
    alignItems: 'center',
    left : -30,
    flexDirection: 'row',
  },
  button: {
    width:180,
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 5,
    paddingVertical: 5
  },
  buttonText: {
    fontSize:12,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
});

class Detail extends Component<{}> {

  logoutUser = () => {
      this.props.dispatch(logoutUser());
  }
  
  home() {
    Actions.home()
}

detail() {
    Actions.detail()
}

	render() {
    const {getUser: {userDetails}} = this.props;

		return(
			<View style={styles.container}>
        <View style={styles.header}>
             <TouchableOpacity style={styles.button} onPress={this.home}>
                <Text style={styles.buttonText}>Home</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={this.logoutUser}>
             <Text style={styles.buttonText}>Logout</Text>
           </TouchableOpacity>
           </View>
              
           <View style={styles.cheader}>
            <Text style={{ fontSize: 24,fontWeight: 'bold',color:'#003366' }}>Detail Barang</Text></View>
            <View style={styles.content}>
              <Icon style={styles.people} name="account-circle" size={100} />
              <Text style={styles.identity}>Nama Barang</Text>
              <Text style={styles.identity3}>Harga Barang</Text>
              <View style={styles.box2}>
                <Text style={styles.identity2}>Deskripsi Barang</Text>
                <Text style={{borderWidth:1,left: 10, width : 300, borderColor:'#003366',marginTop:10, height:0}}></Text>
                <View style={styles.page2}>
                  <View style={styles.mendatar}>
                    <Text style={styles.identity2}>Warna : </Text>
                  </View>
                  <View style={styles.mendatar}>
                    <Text style={styles.identity2}>Ukuran : </Text>
                  </View>
                  <View style={styles.mendatar}>
                    <Text style={styles.identity2}>Spesifikasi : </Text>
                  </View>
                </View>
              </View>
            </View>
                
          </View>
			)
	}
}

mapStateToProps = (state) => ({
    getUser: state.userReducer.getUser
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
