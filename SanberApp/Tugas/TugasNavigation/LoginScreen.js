import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
const Drawer = createDrawerNavigator();
const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const LoginScreen = ({ navigation }) => {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{ width: 330, height: 102, left: 0, top: 43 }} />
        </View>
        <View style={styles.body}>
            <Text style={{ left: 150, top: 125, fontSize: 24 }}>LOGIN</Text>
            <Text style={{ left: 20, top: 155, fontSize: 12 }}>Username / Email</Text>
            <TextInput style={{ width: 320, height: 40, backgroundColor: 'azure', fontSize: 14, left: 20, top : 170  }} placeholder="Tuliskan Username / Email" onChangeText={(text) => this.setState({text})} />
            <Text style={{ left: 20, top: 190, fontSize: 12 }}>Password</Text>
            <TextInput style={{ width: 320, height: 40, backgroundColor: 'azure', fontSize: 14, left: 20, top : 205  }} placeholder="Tuliskan Password" onChangeText={(text) => this.setState({text})} />
            
            <View style={styles.buttonMasuk}>
                <Button onPress={this._onPress} title="Masuk" color="#3EC6FF" accessibilityLabel="Tap on Me"/>
            </View>
            <Text style={{ left: 164, top: 240, fontSize: 20, color: "#3EC6FF" }}>atau</Text>
            <View style={styles.buttonDaftar}>
                <Button onPress={this._onPress} title="Daftar ?" color="#003366" accessibilityLabel="Tap on Me"/>
            </View>
        </View>
        
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  buttonMasuk: {
    backgroundColor: '#2E9298',
    borderRadius: 16,
    width : 150,
    left : 110,
    top : 230,
  },
  buttonDaftar: {
    backgroundColor: '#2E9298',
    borderRadius: 16,
    width : 150,
    left : 110,
    top : 250,
  }
 
});