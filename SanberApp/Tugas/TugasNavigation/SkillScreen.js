import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from './components/skillItem';
import data from './components/skillData.json';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
const Drawer = createDrawerNavigator();
const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);
export const SkillScreen = ({ navigation }) => {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{ width: 110, height: 40 }} />
        </View>
        <View style={styles.header}>
            <Icon style={styles.people} name="account-circle" size={40} />
            <View style={styles.cheader}>
                <Text> Hai,</Text>
                <Text style={{color: '#003366', fontSize:16,}}> Ibrahim Wahyu Saputra</Text>
            </View>
        </View>
        <View style={styles.body}>
            <View>
                <Text style={{color: '#003366', fontSize:36, fontWeight: 'bold',}}> SKILL</Text>
                <Text style={{borderWidth:2, width : 320, borderColor:'#3EC6FF',marginTop:10, height:0}}></Text>
            </View>
            <View style={styles.bodyNav}>
                <View style={styles.bodyNavTitle}>
                    <Text style={styles.bodyTitle}> Library/Frameword</Text>
                    <Text style={styles.bodyTitle}> Bahasa Pemrograman</Text>
                    <Text style={styles.bodyTitle}> Teknologi</Text>
                </View>
                <FlatList style={{height:440,}}
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id.toString()}
                    ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                    />
                   
            </View>
        </View>
        
      </View>
    );
  };


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginTop: 30
  },
  header:{
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  cheader:{
    flexDirection: 'column',
  },
  cheaderText:{
    textAlign:'left',
  },
  people:{
    textAlign:'left',
    marginLeft: 16,
    color : '#003366'
  },
  body: {
    flex: 1,
    marginLeft: 16,
    marginRight:16,
  },
  bodyNavTitle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding : 5,
  },
  bodyTitle: {
    color: '#003366', 
    fontSize:11, 
    backgroundColor: '#B4E9FF', 
    padding:5, 
    margin : 3,
    borderRadius:8,
  },

  
});