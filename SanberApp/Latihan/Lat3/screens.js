//import {Navigation} from 'react-native-navigation';
import { Navigation } from "@react-navigation/native";
import {Provider} from 'react-redux';

import {store} from './store';

export function registerScreens() {

  Navigation.registerComponent('blog.PostsList', () => require('./posts/screens/PostsList').default);
  Navigation.registerComponentWithRedux('blog.PostsList', () => require('./posts/screens/PostsList').default, Provider, store);
  Navigation.registerComponent('blog.AddPost', () => require('./posts/screens/AddPost').default);
  Navigation.registerComponent('blog.ViewPost', () => require('./posts/screens/ViewPost').default);

}