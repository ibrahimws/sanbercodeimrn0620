import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fa from 'react-native-vector-icons/AntDesign';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
const Drawer = createDrawerNavigator();
const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const About = ( navigation ) => {
	  return(
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"white"}
    translucent = {false}/>
	  	<View style={styles.cheader}>
            <Text style={{ fontSize: 32,fontWeight: 'bold',color:'#003366' }}>Tentang Saya</Text></View>
	  	<View style={styles.content}>
	  		<Icon style={styles.people} name="account-circle" size={120} />
	  		<Text style={styles.identity}>Ibrahim Wahyu Saputra</Text>
        <Text style={styles.identity3}>Guru dan Web Developer</Text>
        <View style={styles.box1}>
          <Text style={styles.identity2}>Portofolio</Text>
          <Text style={{borderWidth:1, left: 10, width : 300, borderColor:'#003366',marginTop:10, height:0}}></Text>
          <View style={styles.page1}>
            <View style={styles.satu}>
              <Fa name="gitlab" style={{padding:5,textAlign:'center', left :5,color : '#3EC6FF'}} size={50} />
              <Text style={styles.identity2}>@ibrahimws</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="github" style={{padding:5,textAlign:'center', left: 10,color : '#3EC6FF'}} size={50} />
              <Text style={styles.identity2}>@ibrahimwahyus</Text>
            </View>
          </View>
        </View>
        <View style={styles.box2}>
          <Text style={styles.identity2}>Hubungi Saya</Text>
          <Text style={{borderWidth:1,left: 10, width : 300, borderColor:'#003366',marginTop:10, height:0}}></Text>
          <View style={styles.page2}>
            <View style={styles.mendatar}>
              <Fa name="facebook-square" style={{padding:5,color : '#3EC6FF'}} size={40} />
              <Text style={styles.identity2}>@baiim.as.ibrahim</Text>
            </View>
            <View style={styles.mendatar}>
              <Fa name="instagram" style={{padding:5,color : '#3EC6FF'}} size={40} />
              <Text style={styles.identity2}>@baiim_ibrahim</Text>
            </View>
            <View style={styles.mendatar}>
              <Fa name="twitter" style={{padding:5,color : '#3EC6FF'}} size={40} />
              <Text style={styles.identity2}>@baiim_ibrahim</Text>
            </View>
          </View>
        </View>
	  	</View>
	  	<View style={styles.footer}>
	  		<Text style={{fontWeight:'bold'}}>Copyright By @Student</Text>
	  	</View>
	  </View>
	  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  cheader:{
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:350,
  },
  content:{
  	height:600,
  	padding:20
  },
  box1:{
    marginTop:10,
    backgroundColor:'#EFEFEF',
    height:150,
    width:320,
    borderRadius:10,

  },
  box2:{
    marginTop:10,
    backgroundColor:'#EFEFEF',
    height:200,
    width:320,
    borderRadius:10,

  },
  people:{
    textAlign:'center',
    width:320,
    top:-20,
    color : '#EFEFEF'
  },
  identity:{
    textAlign:'center',
    color:'#003366',
    top : -10,
    fontSize : 24,
    fontWeight: 'bold',
  },
  identity2:{
    textAlign:'left',
    marginTop:0,
    marginLeft:10,
    color:'#003366'
  },
  identity3:{
    textAlign:'center',
    color:'#3EC6FF',
    fontSize : 16,
    top : -10
  },
  page1:{
    width:360,
    height:150,
    padding:10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  page2:{
    width:360,
    height:150,
    left : 90,
    top : 10,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  satu:{
    alignItems: 'center',
    width:170,
    height:150,
    padding:10,
    left : -30,
    textAlign:'center'
  },
  mendatar:{
    alignItems: 'center',
    left : -30,
    flexDirection: 'row',
  },
  footer:{
  	marginTop:50,
  	height: 60,
    backgroundColor: '#57bce7',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
