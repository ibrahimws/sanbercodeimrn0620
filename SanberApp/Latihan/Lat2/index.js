import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {
  SignIn,
  CreateAccount,
  Search,
  Home,
  Details,
  Search2,
  Profile,
} from "./Screen";

import { LoginScreen } from "./LoginScreen"
import { About } from "./AboutScreen"
import { AddScreen } from "./AddScreen"
import { ProjectScreen } from "./ProjectScreen"
import { SkillScreen } from "./SkillScreen"


const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={LoginScreen} />
  </LoginStack.Navigator>
);

const SkillStack = createStackNavigator();
const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={SkillScreen} />
  </SkillStack.Navigator>
);

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={ProjectScreen} />
  </ProjectStack.Navigator>
);

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={AddScreen} />
  </AddStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} />
    <HomeStack.Screen
      name="Details"
      component={Details}
      options={({ route }) => ({
        title: route.params.name
      })}
    />
  </HomeStack.Navigator>
);

const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search} />
    <SearchStack.Screen name="Search2" component={Search2} />
  </SearchStack.Navigator>
);

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="About" component={About} />
  </ProfileStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
    <Tabs.Screen name="Add" component={AddStackScreen} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();


export default () => {
  return (
  <NavigationContainer>
    <Drawer.Navigator initialRouteName="Login">
    <Drawer.Screen name="Tabs" component={TabsScreen} />
    <Drawer.Screen name="About" component={ProfileStackScreen} />
    <Drawer.Screen name="Login" component={LoginStackScreen} />
  </Drawer.Navigator>
  </NavigationContainer>
);
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
