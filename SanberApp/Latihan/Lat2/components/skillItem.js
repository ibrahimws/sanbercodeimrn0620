import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import IconSkill from 'react-native-vector-icons/MaterialCommunityIcons';


export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <View style={styles.descContainer}>
                    <IconSkill name={skill.iconName} style={styles.iconSkill} size={80} color="#003366"/>
                    <View style={styles.skillDetails}>
                        <Text style={styles.skillTitle}>{skill.skillName}</Text>
                        <Text style={styles.skillKategori}>{skill.categoryName}</Text>
                        <Text style={styles.skillProgress}>{skill.percentageProgress}</Text>
                    </View>
                    <TouchableOpacity>
                        <IconSkill style={styles.iconPanah} name="chevron-right" size={60} color="#003366"/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 5,
        left:-5,
    },
    descContainer: {
        flexDirection: 'row',
        padding: 5,
        backgroundColor: '#B4E9FF',
        height: 120,
        width:320,
        borderRadius:8,
    },
    iconSkill:{
        marginTop:15,
    },
    skillDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    skillTitle: {
        fontSize:18,
        color:'#003366',
        fontWeight: 'bold',
    },
    skillKategori:{
        color: '#3EC6FF',
    },
    skillProgress: {
        fontSize:48,
        color : 'white',
        textAlign:'right',
        fontWeight: 'bold',
    },
    iconPanah: {
        marginTop: 25,
    },
});