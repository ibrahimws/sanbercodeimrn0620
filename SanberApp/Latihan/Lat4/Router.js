import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flex';

import Login from './pages/Login';
import Signup from './pages/Signup';

export default class Routes extends Component <{}> {
    render () {
        return(
            <Router>
                <Stack key="root">
                    <Scene key="login" component={Login} title="Login"/>
                    <Scene key="registe" component={Signup} title="Register"/>
                </Stack>
            </Router>
        )
    }
}