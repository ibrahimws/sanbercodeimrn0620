import Logo from 'component/Logo';
import Form from 'component/Form';

import {Actions} from 'react-native-router-flux';

export default class Signup extends Component <{}> {
    goBack() {
        Actions.pop();
    }

    render() {
        return{
            <View style={styles.container}>
                <Logo/>
                <Form type="Signup"/>
                <View style={styles.signupTextCont}>
                    <Text style={styles.SignupText}>Already have an account</Text>
                    <TouchableOpacity onPress={this.goBack}><Text style={styles.SignupButton}>Sign In</Text></TouchableOpacity>
                </View>
            </View>
        }
    }
} 