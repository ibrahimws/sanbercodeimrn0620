//If-else
var nama = "John";
var peran = "";

// Output untuk Input nama = '' dan peran = ''
if ( nama == "" && peran == "" ) {
    console.log("Nama harus diisi!");
}

//Output untuk Input nama = 'John' dan peran = ''
else if ( nama == "John" && peran == "" ) {
    console.log("Halo John, Pilih peranmu untuk memulai game!");
}
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
else if ( nama == "Jane" && peran == "Penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
}
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
else if ( nama == "Jenita" && peran == "Guard" ) {
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
else if ( nama == "Junaedi" && peran == "Werewolf" ) {
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}

//Switch Case
var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var kabisat;

kabisat = tahun % 2;
//cek kondisi variable
if (bulan > 12) {
    console.log("Variabel bulan salah");
}
else if (tahun < 1900 || tahun > 2200) {
    console.log("Variabel tahun salah");
}
else if (tanggal < 1 || tanggal > 31) {
    console.log("Variabel tanggal salah");
}
else if (bulan == 2 && kabisat == 0 && tanggal > 29) 
{   
    console.log("Variabel tanggal salah");
}
else if (bulan == 2 && kabisat == 1 && tanggal > 28) 
{   
    console.log("Variabel tanggal salah");
}
else if ((bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) && (tanggal > 30)) 
{
    console.log("Variabel tanggal salah");
}
else
{
//perubah bulan
switch(bulan) 
{
  case 1:   { bulan = "Januari"; break; }
  case 2:   { bulan = "Februari"; break; }
  case 3:   { bulan = "Maret"; break; }
  case 4:   { bulan = "April"; break; }
  case 5:   { bulan = "Mei"; break; }
  case 6:   { bulan = "Juni"; break; }
  case 7:   { bulan = "Juli"; break; }
  case 8:   { bulan = "Agustus"; break; }
  case 9:   { bulan = "September"; break; }
  case 10:   { bulan = "Oktober"; break; }
  case 11:   { bulan = "November"; break; }
  case 12:   { bulan = "Desember"; break; }
}
console.log(tanggal + " " + bulan + " " + tahun);
}