// SOAL NO 1
console.log("SOAL NO 1")

function range(startNum, finishNum) {
    var urut = [];
    if (startNum>finishNum)
    {
        for(var i=startNum;i>=finishNum;i--) 
        {
            urut.push(i);
        }   
    }
    else if (startNum<finishNum) {
        for(var i=startNum;i<=finishNum;i++) 
        {
            urut.push(i);
        } 
    }
    else
    {
        urut.push(-1);
    }
    return urut;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("\n");
// SOAL NO. 2
console.log("SOAL NO 2")

function rangeWithStep(startNum, finishNum, step) {
    var lompat = [];
    if (startNum>finishNum)
    {
        for(var i=startNum;i>=finishNum;) 
        {
            lompat.push(i);
            i = i-step;
        }   
    }
    else if (startNum<finishNum) {
        for(var i=startNum;i<=finishNum;) 
        {
            lompat.push(i);
            i = i+step;
        } 
    }
    return lompat;
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("\n");
// SOAL NO. 3
console.log("SOAL NO 3")

function sum(startNum, finishNum, step) {
    var lompat = 0;
    if (step == null)
    {
        step = 1;
    }
    if (finishNum == null)
    {
        finishNum = 0;
    }  
    if (startNum>finishNum)
    {
        for(var i=startNum;i>=finishNum;) 
        {
            lompat=lompat + i;
            i = i-step;
        }   
    }
    else if (startNum<finishNum) {
        for(var i=startNum;i<=finishNum;) 
        {
            lompat=lompat + i;
            i = i+step;
        } 
    }
    return lompat;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\n");
// SOAL NO. 4
console.log("SOAL NO 4");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling () 
{
    for (var i=0; i<input.length; i++) 
    {
        console.log("Nomor ID: "+ input[i][0]);
        console.log("Nama Lengkap: "+ input[i][1]);
        console.log("TTL: "+ input[i][2]+ " " + input[i][3]);
        console.log("Hobi: "+ input[i][4]);
        console.log("\n");
    }
}

dataHandling();

console.log("\n");
// SOAL NO. 5
console.log("SOAL NO 5");

function balikKata(string) 
{
    var balik = "";
    for (var i=string.length; i>0 ; i--) 
    {
        balik = balik + string[i-1];
    }
    return balik;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("\n");
// SOAL NO. 6
console.log("SOAL NO 6");

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(data) {
    var data1 = data[1];
    data.splice(1, 1, data[1] + " Elsharawy")
    data.splice(2, 1, "Provinsi" + data[2])
    data.splice(4, 1, "Pria", "SMA Internasional Metro")
    var TTL = data[3].split("/")
    console.log(data);
    switch (Number(TTL[1])) {
        case 01: console.log("Januari"); break;
        case 02: console.log("Februari"); break;
        case 03: console.log("Maret"); break;
        case 04: console.log("April"); break;
        case 05: console.log("Mei"); break;
        case 06: console.log("Juni"); break;
        case 07: console.log("Juli"); break;
        case 08: console.log("Agustus"); break;
        case 09: console.log("September"); break;
        case 10: console.log("Oktober"); break;
        case 11: console.log("November"); break;
        case 12: console.log("Desember"); break;
        default: break;
    }
    //output
    var balikTTL = [TTL[2], TTL[0], TTL[1]]
    console.log(balikTTL);
    console.log(TTL.join("-"));
    console.log(data1);
}
dataHandling2(input);
